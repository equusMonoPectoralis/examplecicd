#!/usr/bin/python3
#-*- coding: utf-8 -*-

import os

def generate():
	matr=matricule.upper()
	head=open(r'head.tex' , 'r')
	foot=open(r'foot.tex' , 'r')
	s=head.read()+"Le Doyen de la Faculté des \\textbf{Sciences Exactes}\\\\\
\n\\\\%\n\
certifie que l'étudiant(e):\\\\\
\n\\\\%\n\
Nom: \\textbf{"+nom.upper()+"}\\\\\
\n\\\\%\n\
Prénom : \\textbf{"+prenom.capitalize()+"}\\\\\
\n\\\\%\n\
Né(e) le : \\textbf{"+date_nais+"}   à : \\textbf{"+ville_nais.capitalize()+"}\\\\\
\n\\\\%\n\
est inscrit(e)  en \\textbf{Deuxième année.}\\\\\
\n\\\\%\n\
Matricule : \\textbf{"+matr+"}\\\\\
\n\\\\%\n\
Domaine : \\textbf{Mathématiques et Informatique}\\\\\
\n\\\\%\n\
Filière : \\textbf{Informatique}\\\\\
\n\\\\%\n\
Spécialité : \\textbf{Génie Logiciel}\\\\\
\n\\\\%\n\
Diplôme préparé : \\textbf{Master}\\\\\
\n\\\\%\n\
\\textbf{Année universitaire : 2015/2016}\
\n\\\\%\n\
\\begin{flushright}\
\\begin{pspicture}(4,0in)\
\\psbarcode{"+matr+"}{includecheck height=0.4}{code128}\
\\end{pspicture}\\\
\n\\\\%\n\
Béjaia, le 25/11/2015\\\\P/Le Doyen\
\\end{flushright}"+foot.read()
	head.close()
	foot.close()
	certificat = open(r''+matr+'.tex' , 'w')
	certificat.write(s)
	certificat.close()

nom=os.getenv("NOM")
prenom=os.getenv("PRENOM")
date_nais=os.getenv("DATE")
ville_nais=os.getenv("VILLE")
matricule=os.getenv("MATRICULE")
generate()
